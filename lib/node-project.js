'use strict';
const lodash = require('lodash');
const data = require('./data');
const utilities = require('./utilities');

/**
 * Check all Node.js files have the same version
 * @param   {String}  basePath
 */
async function checkProject(basePath) {
    // For simplicity, we just load data for all needed files here
    const [
        packageData,
        devData,
        prodData,
        devAppData,
        prodAppData,
        sonarData,
    ] = await data.loadStandardNodeData(basePath);

    // Extract version data from package.json, this will be considered the expected value
    const packageVersion = lodash.map(packageData.version.split(/[.-]/), Number);
    // Extract version data from the other sources
    const versionsToCheck = [];
    versionsToCheck.push(lodash.map(devData.service_version.split(/[.-]/), Number));
    versionsToCheck.push(lodash.map(prodData.service_version.split(/[.-]/), Number));
    versionsToCheck.push(lodash.map(sonarData.projectVersion.split(/[.-]/), Number));

    if (utilities.compareVersions(packageVersion, versionsToCheck)) {
        // app.yaml and config files have a service name with the major version at the end
        const expectedVersion = [packageVersion[0]];
        const versionsToCheck = [];
        // Some services don't have the major version in its name.
        if (prodAppData.service && prodAppData.service.indexOf(`-v${expectedVersion}`) > -1) {
            versionsToCheck.push([utilities.getMajorVersionForService(devAppData.service)]);
            versionsToCheck.push([utilities.getMajorVersionForService(prodAppData.service)]);
            versionsToCheck.push([utilities.getMajorVersionForService(devData.service_deploy_id)]);
            versionsToCheck.push([utilities.getMajorVersionForService(prodData.service_deploy_id)]);

            if (!utilities.compareVersions(expectedVersion, versionsToCheck)) {
                throw new Error(`Mismatched service name versions. Major version is ${packageVersion[0]}. ` +
                    `config/dev/app.yaml has service name ${devAppData.service}, ` +
                    `config/prod/app.yaml has service name ${prodAppData.service}, ` +
                    `config/dev/config.json has service name ${devData.service_deploy_id}, and ` +
                    `config/prod/config.json has service name ${prodData.service_deploy_id}.`);
            }
        }

        // Also test service names and service deploy ids to test if they match
        const appServiceName = utilities.getServiceName(prodAppData.service);
        const otherAppServiceNames = [
            utilities.getServiceName(devAppData.service),
        ];
        if (!utilities.compareServiceNames(appServiceName, otherAppServiceNames)) {
            throw new Error('Mismatched service names. ' +
                `Expected service name is ${appServiceName} in config/prod/app.yaml but ` +
                `config/dev/app.yaml has service name ${otherAppServiceNames[0]}.`);
        }

        const configServiceName = packageData.name;
        const otherConfigServiceNames = [
            devData.service_name,
            utilities.getServiceName(devData.service_deploy_id),
            prodData.service_name,
            utilities.getServiceName(prodData.service_deploy_id),
        ];
        if (!utilities.compareServiceNames(configServiceName, otherConfigServiceNames)) {
            throw new Error('Mismatched service names. ' +
                `Expected service name ${configServiceName} from package.json but ` +
                `config/dev/config.json has service name ${otherConfigServiceNames[0]} ` +
                `and service deploy id ${otherConfigServiceNames[1]}. ` +
                `config/prod/config.json has service name ${configServiceName} ` +
                `and service deploy id ${otherConfigServiceNames[2]}`);
        }

        return {
            version: packageData.version,
            serviceName: packageData.name,
        };
    } else {
        throw new Error(`Package version does not match config versions. Expected ${packageData.version}, ` +
            `got ${devData.service_version} in config/dev/config.json, ` +
            `${prodData.service_version} in config/prod/config.json ` +
            `and ${sonarData.projectVersion} in sonar-project.properties`);
    }
}

module.exports = {
    checkProject: checkProject,
};
