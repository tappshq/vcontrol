'use strict';
const fs = require('fs');
const path = require('path');
const lodash = require('lodash');
const yaml = require('js-yaml');

/**
 *
 * @param path
 * @returns {Promise<any>}
 */
function loadSonarqubeData(path) {
    return new Promise((resolve, reject) => {
        fs.readFile(path, (err, data) => {
            if (err) {
                return reject(err);
            }

            data = data.toString();
            const lines = data.split('\n');
            const dataObject = {};
            lodash.forEach(lines, (line) => {
                const match = line.match(/^sonar\.([\w]*)\s*=\s*(.*)/);
                if (match) {
                    dataObject[match[1]] = match[2];
                }
            });
            resolve(dataObject);
        });
    });
}

/**
 * Load a YAML file and parse the data inside.
 * @param {String}  path    path to the YAML file
 * @returns {Promise<Object>}  returns a promise that resolves to an object with the parsed data
 */
function loadYamlData(path) {
    return new Promise((resolve, reject) => {
        fs.readFile(path, (err, data) => {
            if (err) {
                return reject(err);
            }
            try {
                data = yaml.safeLoad(data);
                return resolve(data);
            } catch(ex) {
                reject(ex);
            }
        });
    });
}

/**
 * Load a json file from a file.
 * @param {String}  path    path to the file, including filetype e.g. './package.json'
 * @returns {Promise<Object>}  a promise that resolves to the JSON Object
 */
function loadJsonData(path) {
    return new Promise((resolve, reject) => {
        fs.readFile(path, (err, data) => {
            if (err) {
                return reject(err);
            }
            try {
                data = JSON.parse(data);
                return resolve(data);
            } catch(ex) {
                reject(ex);
            }
        });
    });
}

function loadStandardNodeData(basePath) {
    return Promise.all([
        loadJsonData(path.join(basePath, 'package.json')),
        loadJsonData(path.join(basePath, 'config/dev/config.json')),
        loadJsonData(path.join(basePath, 'config/prod/config.json')),
        loadYamlData(path.join(basePath, 'config/dev/app.yaml')),
        loadYamlData(path.join(basePath, 'config/prod/app.yaml')),
        loadSonarqubeData(path.join(basePath, 'sonar-project.properties')),
    ]);
}


module.exports = {
    loadJsonData,
    loadSonarqubeData,
    loadYamlData,
    loadStandardNodeData,
};
