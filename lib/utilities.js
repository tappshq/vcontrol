'use strict';
const lodash = require('lodash');

/**
 * Checks that the versions match.
 * @param {Array}   expectedVersion     array with 3 parsed numbers representing the version
 * @param {Array}   versionsToCheck     array of arrays of 3 numbers representing the versions to tes
 * @returns {Boolean}   true if the versions all match, false otherwise.
 */
function compareVersions(expectedVersion, versionsToCheck) {
    return lodash.reduce(expectedVersion, (acc, value, index) => {
        lodash.forEach(versionsToCheck, (otherValue) => {
            acc = acc && value === otherValue[index];
        });
        return acc;
    }, true);
}

/**
 * Get the major version from the end of the service name
 * @param {String}      serviceName     service name with major version
 * @returns {number}    major   version number
 */
function getMajorVersionForService(serviceName) {
    let majorVersion = NaN;
    if (typeof serviceName === 'string') {
        const matches = serviceName.match(/-v(\d+)$/);
        if (matches) {
            return Number(matches[1]);
        }
    }
    return majorVersion;
}


/**
 * Get the service name without the version from the service name
 * @param {String}  serviceName     service name with major version
 * @returns {String}    service name
 */
function getServiceName(serviceName) {
    if (serviceName) {
        const matches = serviceName.match(/^(.*)-v\d+$/);
        if (matches) {
            return matches[1];
        } else {
            return serviceName;
        }
    } else {
        return 'default';
    }
}

/**
 * Compare the name part of the service name with the values in the array.
 * @param {String}          expectedName    expected value
 * @param {Array<String>}   namesToCheck    values to test.
 * @returns {Boolean}   true if they all match, false otherwise.
 */
function compareServiceNames(expectedName, namesToCheck) {
    const namesMatch = lodash.isString(namesToCheck[0]) && lodash.reduce(namesToCheck, (acc, serviceName) => {
        return acc && serviceName === namesToCheck[0];
    }, true);
    return namesMatch && (expectedName === namesToCheck[0] || 'default' === namesToCheck[0]);
}

module.exports = {
    compareVersions,
    compareServiceNames,
    getMajorVersionForService,
    getServiceName,
};
