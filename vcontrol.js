#!/usr/bin/env node
'use strict';
const versionCheckers = {
    node: require('./lib/node-project'),
};

require('yargs')
    .command('check <path>', 'start the server', (yargs) => {
        yargs
            .positional('path', {
                describe: 'path to the project',
                default: './',
            });
    }, (argv) => {
        if (versionCheckers.hasOwnProperty(argv.project)) {
            // eslint-disable-next-line no-console
            if (argv.verbose) console.log(`Checking path:${argv.path}`);

            versionCheckers[argv.project].checkProject(argv.path, argv.verbose)
                .then(resultObj => {
                    // eslint-disable-next-line no-console
                    if (argv.verbose) console.log(`Service ${resultObj.serviceName} is at ${resultObj.version}`);
                    process.exit(0);
                })
                .catch(reason => {
                    // eslint-disable-next-line no-console
                    console.error(reason);
                    process.exit(1);
                });
        } else {
            // eslint-disable-next-line no-console
            console.error(`Project type must be one of: ${Object.keys(versionCheckers)}`);
            process.exit(2);
        }
    })
    .option('verbose', {
        alias: 'v',
        default: false,
    })
    .option('project', {
        alias: 'p',
        default: 'node',
    })
    .argv;
