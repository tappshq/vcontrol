'use strict';
require('chai');
const rewire = require('rewire');


describe('Utilities unit tests', function () {
    const utilities = rewire('../lib/utilities');

    describe('compareVersions', function() {
        it('should return true if all versions match the expected version', function () {
            const expectedVersion = [200, 55, 30];
            const versionsToCheck = [
                [200, 55, 30],
                [200, 55, 30],
                [200, 55, 30],
            ];
            const result = utilities.compareVersions(expectedVersion, versionsToCheck);
            result.should.be.true;
        });

        it('should return true if only matching major versions are passed', function () {
            const expectedVersion = [20];
            const versionsToCheck = [
                [20],
                [20],
                [20],
            ];
            const result = utilities.compareVersions(expectedVersion, versionsToCheck);
            result.should.be.true;
        });

        it('should return false if any version does not match the expected version', function () {
            const expectedVersion = [200, 55, 30];
            const versionsToCheck = [
                [200, 55, 30],
                [200, 54, 30],
                [200, 55, 30],
            ];
            const result = utilities.compareVersions(expectedVersion, versionsToCheck);
            result.should.be.false;
        });
    });

    describe('getMajorVersionForService', function () {
        it('should return the major version at the end of the service', function () {
            const majorVersion = utilities.getMajorVersionForService('service-name-v24');
            majorVersion.should.equal(24);
        });

        it('should return NaN if the service name is default', function () {
            const majorVersion = utilities.getMajorVersionForService('default');
            majorVersion.should.be.NaN;
        });
    });

    describe('getServiceName', function () {
        it('should return the service name for a service deploy id', function () {
            const serviceName = utilities.getServiceName('service-name-v23');
            serviceName.should.equal('service-name');
        });

        it('should return the service name for a service name without a version', function () {
            const serviceName = utilities.getServiceName('service-name');
            serviceName.should.equal('service-name');
        });

        it('should return default for a service name without a version', function () {
            const serviceName = utilities.getServiceName('default');
            serviceName.should.equal('default');
        });
    });

    describe('compareServiceNames', function () {
        it('should return true if all service names match the expected name', function () {
            const expectedServiceName = 'service-name';
            const serviceNamesToCheck = [
                'service-name',
                'service-name',
                'service-name',
                'service-name',
            ];
            const serviceName = utilities.compareServiceNames(expectedServiceName, serviceNamesToCheck);
            serviceName.should.be.true;
        });

        it('should return false if any service names do not match the expected name', function () {
            const expectedServiceName = 'service-name';
            const serviceNamesToCheck = [
                'service-name',
                'service-name',
                'default',
                'service-name',
            ];
            const serviceName = utilities.compareServiceNames(expectedServiceName, serviceNamesToCheck);
            serviceName.should.be.false;
        });
    });
});
