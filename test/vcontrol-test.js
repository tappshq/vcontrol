'use strict';
require('chai');
const {spawn} = require('child_process');

describe('vcontrol command line', function () {
    it('should test a node project when the check command is run with the option node', function (done) {
        const runCheck = spawn('./vcontrol.js', ['check', './test/node-test-data/valid-project', '-p', 'node', '-v']);

        runCheck.stdout.on('data', function (data) {
            // eslint-disable-next-line no-console
            console.log(data.toString());
        });

        runCheck.stderr.on('data', function (data) {
            // eslint-disable-next-line no-console
            console.log(data.toString());
        });

        runCheck.on('close', function (code) {
            code.should.equal(0);
            done();
        });
    });

    it('should fail when the check is for a incorrect project', function (done) {
        const runCheck = spawn('./vcontrol.js', ['check', './test/node-test-data/invalid-projects/wrong-service', '-p', 'node']);

        runCheck.stdout.on('data', function (data) {
            // eslint-disable-next-line no-console
            console.log(data.toString());
        });

        runCheck.stderr.on('data', function (data) {
            // eslint-disable-next-line no-console
            console.log(data.toString());
        });

        runCheck.on('close', (code) => {
            code.should.equal(1);
            done();
        });
    });

    it('should fail when the project type is incorrect', function (done) {
        const runCheck = spawn('./vcontrol.js', ['check', './test/node-test-data/valid-project', '-p', 'java', '-v']);

        runCheck.stdout.on('data', function (data) {
            // eslint-disable-next-line no-console
            console.log(data.toString());
        });

        runCheck.stderr.on('data', function (data) {
            // eslint-disable-next-line no-console
            console.log(data.toString());
        });

        runCheck.on('close', (code) => {
            code.should.equal(2);
            done();
        });
    });
});
