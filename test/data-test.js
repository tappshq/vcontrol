'use strict';
const path = require('path');
const chai = require('chai');
const should = chai.should();
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
chai.use(sinonChai);
const rewire = require('rewire');
const validProjectData = require('./node-test-data/valid-project-data');

describe('Data loading tests', function () {
    const data = rewire('../lib/data');
    describe('loadJsonData', function () {
        it('should return an object representing the data in the file', async function () {
            const loadedData = await data.loadJsonData(path.resolve('./test/node-test-data/valid-project/package.json'));
            loadedData.should.deep.equal(validProjectData.validProjectPackageData);
        });

        it('should throw an error if the file is not found', async function () {
            try {
                const value = await data.loadJsonData(path.resolve('./test/node-test-data/made-up-project/package.json'));
                should.not.exist(value);
            } catch (ex) {
                ex.code.should.equal('ENOENT');
            }
        });

        it('should throw an error if the file is not a valid JSON document', async function () {
            try {
                const value = await data.loadJsonData(path.resolve('./test/node-test-data/valid-project/malformed-package.json'));
                should.not.exist(value);
            } catch (ex) {
                ex.message.should.equal('Unexpected end of JSON input');
            }
        });
    });

    describe('loadYamlData', function () {
        it('should return an object representing the YAML data in the file', async function () {
            const loadedData = await data.loadYamlData(path.resolve('./test/node-test-data/valid-project/config/dev/app.yaml'));
            loadedData.should.deep.equal(validProjectData.validProjectDevAppData);
        });

        it('should throw an error if the file is not found', async function () {
            try {
                const value = await data.loadYamlData(path.resolve('./test/node-test-data/made-up-project/app.yaml'));
                should.not.exist(value);
            } catch (ex) {
                ex.code.should.equal('ENOENT');
            }
        });

        it('should throw an error if the file is not a valid JSON document', async function () {
            try {
                const value = await data.loadYamlData(path.resolve('./test/node-test-data/valid-project/malformed-app.yaml'));
                should.not.exist(value);
            } catch (ex) {
                ex.name.should.equal('YAMLException');
            }
        });
    });

    describe('loadSonarqubeData', function () {
        it('should return an object representing the properties file data in the file', async function () {
            const loadedData = await data.loadSonarqubeData(path.resolve('./test/node-test-data/valid-project/sonar-project.properties'));
            loadedData.should.deep.equal(validProjectData.validProjectSonarData);
        });

        it('should throw an error if the file is not found', async function () {
            try {
                const value = await data.loadSonarqubeData(path.resolve('./test/node-test-data/made-up-project/sonar-project.properties'));
                should.not.exist(value);
            } catch (ex) {
                ex.code.should.equal('ENOENT');
            }
        });

        it('should return an empty object if format is incorrect', async function () {
            const value = await data.loadSonarqubeData(path.resolve('./test/node-test-data/valid-project/malformed.properties'));
            value.should.deep.equal({});
        });
    });

    describe('loadStandardNodeData', function () {
        let mockLoadJson, mockLoadYaml, mockLoadSonar;
        let revertJson, revertYaml, revertSonar;
        before(function () {
            mockLoadJson = sinon.stub();
            revertJson = data.__set__('loadJsonData', mockLoadJson);
            mockLoadYaml = sinon.stub();
            revertYaml = data.__set__('loadYamlData', mockLoadYaml);
            mockLoadSonar = sinon.stub();
            revertSonar = data.__set__('loadSonarqubeData', mockLoadSonar);
        });

        it('should load all the config files from the standard locations', async function () {
            const basePath = './project-path';
            const packagePath = path.join(basePath, 'package.json');
            const devConfigPath = path.join(basePath, 'config/dev/config.json');
            const prodConfigPath = path.join(basePath, 'config/prod/config.json');
            const devAppPath = path.join(basePath, 'config/dev/app.yaml');
            const prodAppPath = path.join(basePath, 'config/prod/app.yaml');
            const sonarPath = path.join(basePath, 'sonar-project.properties');

            data.loadStandardNodeData(basePath);
            mockLoadJson.should.have.been.calledWithExactly(packagePath);
            mockLoadJson.should.have.been.calledWithExactly(devConfigPath);
            mockLoadJson.should.have.been.calledWithExactly(prodConfigPath);
            mockLoadYaml.should.have.been.calledWithExactly(devAppPath);
            mockLoadYaml.should.have.been.calledWithExactly(prodAppPath);
            mockLoadSonar.should.have.been.calledWithExactly(sonarPath);
        });

        after(function () {
            revertSonar();
            revertYaml();
            revertJson();
        });
    });
});
