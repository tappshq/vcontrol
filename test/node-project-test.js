'use strict';
const chai = require('chai');
const should = chai.should();
const sinonChai = require('sinon-chai');
chai.use(sinonChai);
const rewire = require('rewire');

describe('Node.js project unit tests', function () {
    const nodeProject = rewire('../lib/node-project');
    describe('checkProject', function () {
        it('should return true if all the versions and service names in the project match', async function () {
            const {serviceName, version} = await nodeProject.checkProject('./test/node-test-data/valid-project');
            serviceName.should.equal('test-service');
            version.should.equal('55.600.10');
        });

        it('should return true if all the versions match, but the project name is default and does not match package.json', async function () {
            const {serviceName, version} = await nodeProject.checkProject('./test/node-test-data/valid-default-project');
            serviceName.should.equal('test-service');
            version.should.equal('55.600.10');
        });

        it('should throw an error if any versions do not match', async function () {
            try {
                const result = await nodeProject.checkProject('./test/node-test-data/invalid-projects/wrong-version');
                should.not.exist(result);
            } catch(ex) {
                should.exist(ex);
                ex.message.should.equal('Package version does not match config versions. Expected 55.600.10, got 55.600.10 in config/dev/config.json, 55.600.10 in config/prod/config.json and 55.599.20 in sonar-project.properties');
            }
        });

        it('should throw an error if any service names in app yaml do not match', async function () {
            try {
                const result = await nodeProject.checkProject('./test/node-test-data/invalid-projects/wrong-service-yaml');
                should.not.exist(result);
            } catch(ex) {
                should.exist(ex);
                ex.message.should.equal('Mismatched service names. Expected service name is other-service-name in config/prod/app.yaml but config/dev/app.yaml has service name service-name.');
            }
        });


        it('should throw an error if any service names do not match in json files', async function () {
            try {
                const result = await nodeProject.checkProject('./test/node-test-data/invalid-projects/wrong-service');
                should.not.exist(result);
            } catch(ex) {
                should.exist(ex);
                ex.message.should.equal('Mismatched service names. Expected service name other-test-service from package.json but config/dev/config.json has service name test-service and service deploy id test-service. config/prod/config.json has service name other-test-service and service deploy id test-service');
            }
        });

        it('should throw an error if service is not default and app.yaml and config files have different services', async function () {
            try {
                const result = await nodeProject.checkProject('./test/node-test-data/invalid-projects/service-mismatch');
                should.not.exist(result);
            } catch(ex) {
                should.exist(ex);
                ex.message.should.equal('Mismatched service names. Expected service name is other-service-name in config/prod/app.yaml but config/dev/app.yaml has service name service-name.');
            }
        });

        it('should throw an error if any service name major versions do not match', async function () {
            try {
                const result = await nodeProject.checkProject('./test/node-test-data/invalid-projects/wrong-major');
                should.not.exist(result);
            } catch(ex) {
                should.exist(ex);
                ex.message.should.equal('Mismatched service name versions. Major version is 55. config/dev/app.yaml has service name test-service-v54, config/prod/app.yaml has service name test-service-v55, config/dev/config.json has service name test-service-v55, and config/prod/config.json has service name test-service-v55.');
            }
        });

    });
});
