module.exports = {
    validProjectPackageData: {
        name: 'test-service',
        description: 'description',
        version: '55.600.10',
        private: true,
        engines: {
            node: '>=8.9.4',
        },
        dependencies: {
            bcrypt: '^3.0.0',
            uuid: '^3.3.2',
        },
        devDependencies: {
            chai: '^4.1.2',
        },
        scripts: {
            start: 'node index.js',
        },
    },
    validProjectDevAppData: {
        service: 'test-service-v55',
        env: 'flex',
    },
    validProjectSonarData: {
        projectName: 'Valid Project',
        projectVersion: '55.600.10',
        key: 'value',
    },
};
