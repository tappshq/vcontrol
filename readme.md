# vcontrol

## About
Application to verify (and eventually update) version data in TPServer services.
The code is in node and can be used as a standalone executable to check the services in
the GOCD test and deploy pipeline or as a unit test in the project itself.

## Installing
To install the package run
```
npm install git+https://bitbucket.org/tappshq/vcontrol
```


## Running
To test a project run the command
```
node <path to node_modules>/vcontrol check <path to project>
```
For example, to test the Node.js project that is in the current working directory, run:
```
node ./node-modules/vcontrol check ./
```

For help using the program, run with the ```--help``` option

## Using for testing
When vcontrol is installed, it can be used as any other node dependency. Below is an example unit
test to check the version data for a Node.js project.

```
'use strict';
const versionCheck = require('vcontrol/lib/node-project');
const chai = require('chai');
chai.should();

describe("Version checker call", function () {
    it("should verify the versions in the project files", async function() {
        await versionCheck.checkProject('./');
    });
});
```


## Version
Latest version is 1.0.4
